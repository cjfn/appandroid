package com.example.troy.serviciosandroidphp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btningresar;
    EditText txtusu, txtpas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtusu=(EditText)findViewById(R.id.txtusu);
        txtpas=(EditText)findViewById(R.id.txtpas);
        btningresar=(Button)findViewById(R.id.btningresar);
        btningresar.setOnClickListener(this);
    }

    public String enviarDatosGet(String usu, String pas)
    {
        URL url=null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try
        {
            url = new URL("http://wbinnovacionreal.com/crud/android/valida.php?usu="+usu+"&pas="+pas);
            HttpURLConnection conection=(HttpURLConnection)url.openConnection();
            respuesta=conection.getResponseCode();

            resul=new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in=new BufferedInputStream(conection.getInputStream());
                BufferedReader reader=new BufferedReader(new InputStreamReader(in));

                while((linea=reader.readLine())!=null){
                    resul.append(linea);
                }
            }

        }catch (Exception e){}

        return resul.toString();

    }

    public int obtDatosJSON(String response){
        int res=0;
        try{
            JSONArray json=new JSONArray(response);
            if(json.length()>0){
                res=1;
            }
        }catch(Exception e){}
        return res;
    }

    @Override
    public void onClick(View v) {
        Thread tr=new Thread() {//hilo
            @Override
            public void run() {
                final String resultado=enviarDatosGet(txtusu.getText().toString(),txtpas.getText().toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int r=obtDatosJSON(resultado);//veo el json
                        if(r>0)//si es mayor  0 el array
                        {
                            Intent i=new Intent(getApplicationContext(),registroNotas.class);//instancio nuevo activity
                            i.putExtra("cod",txtusu.getText().toString());
                            startActivity(i);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"usuario o pass incorrectos",Toast.LENGTH_LONG).show();
                        }
                    }
                });//metodo para interfaz grafica
            }
        };
        tr.start();
    }
}
